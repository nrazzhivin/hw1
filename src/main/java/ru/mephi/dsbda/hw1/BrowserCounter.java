package ru.mephi.dsbda.hw1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/** Class to run a task to count the number of users of each browser based on  log file  */

public class BrowserCounter {
	

    public static void main(String[] args) throws IOException,
            InterruptedException, ClassNotFoundException { 

        // Create a configuration
        Configuration conf = new Configuration(true);

        // Creating a task
        Job job = Job.getInstance(conf, "log request size");
        job.setJarByClass(BrowserCounterMapper.class);

        // Customize MapReduce
        job.setMapperClass(BrowserCounterMapper.class);
        job.setCombinerClass(BrowserCounterReducer.class);
        job.setReducerClass(BrowserCounterReducer.class);
        job.setNumReduceTasks(1);

        // Specifying types of keys and values
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        Path inputPath = new Path(args[0]);
        Path outputDir = new Path(args[1]);
        
        // Configure input data
        FileInputFormat.addInputPath(job, inputPath);
        job.setInputFormatClass(TextInputFormat.class);

        // Configure output data
        FileOutputFormat.setOutputPath(job, outputDir);
        conf.set("mapreduce.output.textoutputformat.separator", ";");

        // Removing old output files
        FileSystem hdfs = FileSystem.get(conf);
        if (hdfs.exists(outputDir))
            hdfs.delete(outputDir, true);

        // Task execution
        int code = job.waitForCompletion(true) ? 0 : 1;
        System.exit(code);

        
        
        
    }

}
