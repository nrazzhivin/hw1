package ru.mephi.dsbda.hw1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/** Reducer tasks for counting the number of users of each browser based on the log file  */

public class BrowserCounterReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

    public void reduce(Text browser, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {
        int count = 0;
        for (IntWritable value : values) {
            count += value.get();
        }
        context.write(browser, new IntWritable(count));
    }

}