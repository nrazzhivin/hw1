package ru.mephi.dsbda.hw1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/** Mapper tasks for count the number of users of each browser based on log file */


public class BrowserCounterMapper extends Mapper<Object, Text, Text, IntWritable> {

    private final IntWritable Count = new IntWritable(1);

    // Browsers
    private static final String FIREFOX = "Mozilla Firefox";
    private static final String CHROME = "Chrome";
    private static final String SAFARI = "Safari";
    private static final String OPERA = "Opera";
    private static final String IE = "Internet Explorer";
    private static final String UNDEFINED = "Undefined Browser";
    
  //  Fragments of strings, which determines the client's browser
    
    private static final String FIREFOX_INCLUDES = "Firefox/";
    private static final String CHROME_INCLUDES = "Chrome/";
    private static final String SAFARI_INCLUDES = "Safari/";
    private static final String OPERA_INCLUDES = "Opera/";
    private static final String IE_INCLUDES = "MSIE";


    public void map(Object key, Text value, Context context)
            throws IOException, InterruptedException {

        String[] properties = value.toString().split("\"");

        String userAgent = properties[properties.length-1];
        Text browser;
        if (userAgent.contains(IE_INCLUDES))
            browser = new Text(IE);
        else if (userAgent.contains(OPERA_INCLUDES))
            browser = new Text(OPERA);
        else if (userAgent.contains(CHROME_INCLUDES))
            browser = new Text(CHROME);
        else if (userAgent.contains(SAFARI_INCLUDES))
            browser = new Text(SAFARI);
        else if (userAgent.contains(FIREFOX_INCLUDES))
            browser = new Text(FIREFOX);
        else
            browser = new Text(UNDEFINED);
        context.write(browser, Count);
    }
}