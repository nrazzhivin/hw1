package ru.mephi.dsbda.hw1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;


/** MrUnit-test for class "BrowserCounter"   */

public class BrowserCounterTest {

/** Class for testing Map  */
	
	MapDriver<Object, Text, Text, IntWritable> mapDriver;

/** Class for testing Reduce */

	ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;

/** Class for testing MapReduce */

	MapReduceDriver<Object, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;

/** Initialization    */

@Before
public void setUp(){
	BrowserCounterMapper mapper = new BrowserCounterMapper();
	BrowserCounterReducer reducer = new BrowserCounterReducer();
	mapDriver = MapDriver.newMapDriver(mapper);
	reduceDriver = ReduceDriver.newReduceDriver(reducer);
	mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
}

/** Testing Map   */

@Test
public void testMapper() throws IOException {
    mapDriver.withInput(new Object(), new Text("Safari/"));
    mapDriver.withOutput(new Text("Safari"), new IntWritable(1));
    mapDriver.runTest();
}

/** Testing Reduce   */

@Test
public void testReducer() {
  List<IntWritable> values = new ArrayList<IntWritable>();
  values.add(new IntWritable(1));
  values.add(new IntWritable(1));
  reduceDriver.withInput(new Text("Chrome"), values);
  reduceDriver.withOutput(new Text("Chrome"), new IntWritable(2));
  reduceDriver.runTest();
}

/** Testing MapReduce   */

@Test
public void testMapReduce() throws IOException {
mapReduceDriver.withInput(new IntWritable(), new Text(
"Safari/"));
mapReduceDriver.withOutput(new Text("Safari"), new IntWritable(1));
mapReduceDriver.runTest();
}

}